.. SPDX-License-Identifier: CC-BY-SA-4.0

==========
References
==========

.. _references:

.. list-table:: 
  :widths: 25 75
  :class: longtable

  * - **[9PFS]**
    - `9PFS - A virtualization aware File System pass-through <https://www.researchgate.net/publication/228569314_VirtFS--A_virtualization_aware_File_System_pass-through>`_
  * - **[DeviceTree-Chosen]** 
    - `How to pass explicit data from firmware to OS, e.g. kaslr-seed <https://github.com/devicetree-org/dt-schema/blob/main/dtschema/schemas/chosen.yaml>`_
  
      | `Linux specifics <https://elixir.bootlin.com/linux/latest/source/Documentation/devicetree/usage-model.rst>`_
  * - **[RFC 2119]** 
    - `RFC2119.txt <https://www.ietf.org/rfc/rfc2119.txt>`_
  * - **[RPMB]**
    -  `OPTEE description <https://xen-troops.github.io/papers/optee-virt-rpmb.pdf>`_
       `Linaro information <https://lists.linaro.org/pipermail/tee-dev/2020-January/001413.html>`_
  * - **[SBSA]**
    - `Server Base System Architecture 7.0 <https://developer.arm.com/docs/den0029/latest>`_
  * - **[SCMI]**
    - `System Control and Management Interface, v 2.0 or later if not otherwise specified <http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.den0056b/index.html>`_
  * - **[SCMI3]**
    - `System Control and Management Interface, v3.0 <https://developer.arm.com/documentation/den0056/c/?lang=en>`_
  * - **[SCMI-IIO]**
    - `SCMI Linux kernel driver <https://lwn.net/Articles/844752/>`_
  * - **[VHOST-DEVICE]**
    - `Rust based vhost-user backends for VirtIO <https://github.com/rust-vmm/vhost-device/>`_
  * - **[VIRTIO]**
    - `OASIS Virtual I/O Device (VIRTIO) Version 1.3, Committee Specification 01, released 6 October 2023 <https://docs.oasis-open.org/VIRTIO/VIRTIO/v1.3/VIRTIO-v1.3.html>`_
  * - **[VIRTIO-CRYPTO]**
    - `OASIS Virtio Crypto chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-4270009>`_
  * - **[VIRTIO-FS]**
    - `OASIS Virtio Filesystem chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-49600011>`_
      `Virtio FS on GitLab <https://VIRTIO-fs.gitlab.io/>`_
  * - **[VIRTIO-GPIO]**
    - `OASIS Virtio GPIO chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-66900018>`_
  * - **[VIRTIO-GPU]**
    - `OASIS Virtio GPU chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-3960007>`_
  * - **[VIRTIO-IOMMU]**
    - `OASIS Virtio IOMMU chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-49600011>`_
  * - **[VIRTIO-RPMB]**
    - `OASIS Virtio RPMB chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-51600012>`_
  * - **[VIRTIO-SCMI]**
    - `OASIS Virtio SCMI chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-53100013>`_
  * - **[VIRTIO-SMR]**
    - `OASIS Virtio Shared Memory Regions <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-10200010>`_
  * - **[VIRTIO-SND]**
    - `OASIS Virtio Sound chapter <https://docs.oasis-open.org/virtio/virtio/v1.3/csd01/virtio-v1.3-csd01.html#x1-56700014>`_
  * - **[VIRTIO-VIRGL]**
    - `non OASIS VIRTIO-VIRGL description <https://github.com/Keenuts/VIRTIO-GPU-documentation/blob/master/src/VIRTIO-GPU.md>`_
  * - **[VIRTIO-VULKAN]**
    -  `Freedesktop Vulkan description <https://gitlab.freedesktop.org/virgl/virglrenderer/-/milestones/2>`_
