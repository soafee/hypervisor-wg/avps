.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
IOMMU Device
------------

**Discussion:**

A Memory Management Unit (MMU) provides virtual memory and allows an
operating system to assign specific memory chunks to processes and thus
allows building memory protection between processes.

An IOMMU provides similar means for hardware devices other than the CPU
cores. A device which can do Direct Memory Accesses (DMA) can access and
modify memory on its own. If the device is driven by an untrusted
software component, e.g. an untrusted VM, or the device itself is not
trusted, the hypervisor needs hardware means to guard the memory
accesses of the device such that it can only access the memory areas
which it is supposed to access. Guarding device memory accesses is also
useful for potentially malfunctioning or misbehaving devices, e.g., due
to bugs in their firmware or electrical glitches, such that the system
is protected and can react accordingly. In this regard an IOMMU acts as
an additional line of protection in a system, especially valuable in
safety-conscious environments.

Overall, an IOMMU provides the kind of protection and separation for
devices required for reliability, functional safety and cyber-security.

.. _iommu:
.. figure:: iommu_images/Figure-1.png
  :align: center

  IOMMU block in a SoC [#]_.

In practice this means that each DMA-capable device needs to be guarded
by an IOMMU-unit that filters memory accesses done by this device. This
also includes bus systems, such as PCIe, that are integrated in the
system and requires that each such device on this bus has its own
IOMMU-unit so that each of those devices can be potentially given to
different untrusted VMs.

Devices that are virtualization-aware, i.e. that can provide separate
distinct interfaces for VMs such as virtual functions (VFs) in SR-IOV,
need an IOMMU-unit for each VF. Devices that are not
virtualization-aware but shall be used among distrusting VMs must be
multiplexed by the hypervisor such that the hypervisor ensures that only
one VM can access the device at a time. This includes reprogramming the
IOMMU settings for these devices on each switch of a VM and reloading
the device configuration for the VM to be switched to. Also this likely
implies adjustments to the VM scheduler in order not to switch to a new
VM until the device really finishes work (completes memory accesses) for
the current VM. Examples can be devices such as accelerators and DSPs.
Due to the complex implementation of this switching it is recommended to
only do this for coarse grained switching use-cases, or to implement a
multiplexing of the device on a higher level.

Since there are different implementations of hardware mechanisms to
protect the platform, this specification requires generically that the
platform and hypervisor provide a solution for memory protection for
co-processors, PCIe-devices, DMA-capable hardware blocks and other
similar devices, such that all devices that can be configured,
controlled, or programmed by untrusted VMs running on the
general-purpose CPUs, are also limited to only the memory that the
controlling VM shall have access to.

Generally, platforms need to be built such that IOMMUs are placed in
front of each memory-accessing device in a granularity allowing to pass
devices to different VMs. This includes interconnects such as PCIe where
multiple independent devices can be hosted. The DMA-capable devices
which are physically connected to the same IOMMU-unit (a situation where
the same IOMMU context is shared between multiple devices) must not be
assigned to different VMs. If such devices are present in the platform
and need to be used for hardware pass-through they can only be assigned
to the same VM to avoid security issues.

:underline:`2-stage IOMMUs, or controlling IOMMU-contexts from the guest`

In a basic implementation, an IOMMU is transparent for the guest VM, as
the IOMMU is solely controlled by the hypervisor and configured for all
devices a VM has access to. Any misconfiguration of devices, or
misbehavior, or malicious behavior will be detected by the IOMMU and
handled by the hypervisor accordingly.

However, an IOMMU can be useful for an operating system kernel itself,
and in virtualization contexts this would require that an IOMMU could
also be used and programmed in the guest VM context. Some IOMMUs on Arm
can provide two separate stages of address translation (e.g. Renesas
IPMMU-VMSA and some versions of Arm SMMU). This makes it possible to
configure the IOMMU in way where the first context (page table) is used
for stage-1 address translation in the guest (VA-IPA) and the second
context (page table) is used for stage-2 address translation (IPA-PA) in
the hypervisor. In effect, both the hypervisor and the guest VM can
program the IOMMU.

This requires some emulation by the hypervisor if the IOMMU control
registers for both stages are located within the same memory page (4K).
While the page-table can be managed by the guest exclusively, the access
to control registers must be trapped by the hypervisor and properly
emulated. When control registers are shared we don’t want to let guests
control IOMMU without validating the operations or an untrusted guest
would be able to disable the IOMMU in order to bypass it, or re-program
it to use for example the context of another VM.

Finally, in the case the IOMMU does not offer two stages or there is no
corresponding support in the hypervisor to emulate guest accesses to the
IOMMU control registers, then the hypervisor can offer the guest a fully
emulated (virtual) IOMMU and let the VM program that via the
VIRTIO-IOMMU protocol. This is usually recommended over a 2-stage
emulation.

Some uses of a 2-stage IOMMU are:

- The guest can limit the access of a device to an even smaller part of
  its own memory as part of making its software more resistant and
  detecting bugs in the guest system software.

- Provide devices contiguous memory that is built out of physically
  scattered memory pages of VM memory.

- Access devices that are limited in address range, such as 32bit devices
  on 64bit hosts. With a 2-stage IOMMU the guest can selectively assign
  those memory pages to the 32bit device that shall be used and the guest
  can adapt this at runtime according to its buffer allocation strategy.
  This is generally useful but other
  options exist when the hypervisor can give the guest multiple areas of
  physical memory where one of those areas is within the reach of the
  device and the guest allocates buffer this device out of this area.

Left out of scope of this specification are use-cases where
guest-programmable IOMMUs are used for other hybrid implementations. One
example is an implementation that adds virtualization to Linux
containers, such as the Kata Containers project.

It was also decided that nested virtualization is not a required feature
in the context of this specification.

In conclusion, AVPS therefore only states an optional requirement for
reprogramming the IOMMU from the guest because the uses for second-stage
IOMMU summarized above are primarily for development support, may have
other workarounds, or is needed for nested virtualization which we do
not require. An optional requirement applies some conditions if this
optional feature is included.

Some SoC solutions have additional memory-related features to support
memory and I/O protection in security and or safety contexts.
Bus-masters and bus-slaves (for example connected to AXI and peripheral
buses) can be assigned to security and safety groups. Examples of
bus-masters are System/Application CPU, Realtime CPU, GPU. Access
limitations are set up to control memory access between groups. For
example you could allow only the Realtime CPU to access CAN. These
controls may be further described in a future version of the
specification.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-IOMMU-1}
    - The virtualization system must protect the system from
      untrusted, malfunctioning or buggy devices or devices driven by
      untrusted VMs.
  * - {AVPS-IOMMU-2}
    - Support for a guest-programmable IOMMU is an optional
      feature with regards to this specification.
  * - {AVPS-IOMMU-3}
    - If the virtual platform implements guest-control of IOMMU then:
   
      * It shall use [VIRTIO-IOMMU]
      * or
      * If the hardware has support for two stage IOMMU the Virtual Platform
        may provide guest programming of a separate stage instead if the
        hypervisor emulates access to IOMMU control registers, if this is
        required to ensure full separation and avoiding interference betwee VMs.

.. todo:: A chapter on DSP / co-processors may have additional requirements related to IOMMU.

.. rubric:: Footnotes

.. [#] Image from Wikimedia Commons, License: Public Domain
