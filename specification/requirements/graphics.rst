.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Graphics
--------

Introduction
~~~~~~~~~~~~~~~~~~~~~

The Graphics Processing Unit is one of the first and most commonly
considered shared functionality when placing multiple VMs on a single
hardware, and yet it is likely the most challenging. Standard
programming APIs are relatively stable for 2D, but significant progress
and change happens in the 3D programming standards, as well as feature
growth of GPUs, especially for built-in virtualization support.

GPU Device in 2D Mode
~~~~~~~~~~~~~~~~~~~~~

VIRTIO-GPU is appropriate and applicable for 2D graphics but using the
3D mode only is more common these days.

In the unaccelerated 2D mode there is no support for DMA transfers
from resources, just to them. Resources are initially simple 2D resources, consisting of a
width, height and format along with an identifier. The guest must then
attach a backing store to the resources for DMA transfers to work.

When attaching buffers use pixel format, size, and other metadata for
registering the stride. With uncommon screen resolutions, this might be
unaligned, and some custom strides might be needed to match.

**AVPS Requirements**: for 2D Graphics

**Device ID.**

.. list-table:: 
  :widths: 20 80

  * - {AVPS-GFX-1}
    - The device ID MUST be set according to the requirement in chapter 5.7.1 in [VIRTIO-GPU].

**Virtqueues.**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-2}
    - The virtqueues MUST be set up according to the requirement in chapter 5.7.2 in [VIRTIO-GPU].

**Feature bits.**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-3}
    - The VIRTIO_GPU_F_VIRGL flag, described in chapter 5.7.3 in [VIRTIO-GPU], MUST NOT be set.
  * - {AVPS-GFX-4}
    - The VIRTIO_GPU_F_EDID flag, described in chapter 5.7.3 in [VIRTIO-GPU], MUST be set and supported to allow the guest to use display size to calculate the DPI value.

**Device configuration layout.**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-5}
    - The implementation MUST use the device configuration layout according to chapter 5.7.4 in [VIRTIO-GPU]. 

      * The implementation MUST NOT touch the reserved structure field as it is used for the 3D mode.

**Device Operation.**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-6#1}
    - The implementation MUST support the device operation concept (the command set and the operation flow) according to chapter 5.7.6 in [VIRTIO-GPU].

      * The implementation MUST support scatter-gather operations to fulfil the requirement in chapter 5.7.6.1 in [VIRTIO-GPU].
      * The implementation MUST be capable to perform DMA operations to client's attached resources to fulfil the requirement in chapter 5.7.6.1 in [VIRTIO-GPU].

**VGA Compatibility.**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-7}
    - VGA compatibility, as described in chapter 5.7.7 in [VIRTIO-GPU], is optional.

GPU Device in 3D Mode
~~~~~~~~~~~~~~~~~~~~~

**Discussion**:

There is ongoing development in 3D APIs and this impacts setting a
standard for virtualization of 3D graphics. In addition, it is desirable to make use of hardware support
for virtualization in modern SoCs to get further improved isolation/separation and higher performance,
compared to a more abstract virtual graphics API.

**Input requirements for GPU Virtualization**

Since this is a particular challenging area we go through a list of system design considerations:

- Security
- Safety
- Policy / QoS / Performance isolation / Resource reservation
- Power Management
- Boot flow
- Hypervisor implementation
- Portability (Upgrading of HW and SW) shall be easy/efficient. The base architecture shall be common enough such that components can be upgraded independently.
- Licensing of code involved

Hardware blocks:

- (GP)GPU
- Framebuffer Composition
- IOMMU / GIC ITS (isolated device memory, isolated device MSI handling)
  
Security:

- Each queue-pair needs to have an IOMMU unit attached
- Partitioning vs. covert channels
- Memory bus separation

Safety:

- if safety-relevant use-case included

  - Targets all HW blocks (GPU, Composition, CPU/GIC) oPriority of request handling
  - Watchdog for progress and/or output results of graphics?
  
Generic hardware virtualization requirements / issues:

- Multiple queues to pass to clients
- At least one device interrupt pre client
- Partitioning of HW (e.g. for GPU, DSP, …)
- Policy / configuration of queue handling
- QoS / queue scheduling, covert channels vs. partitioning
- IOMMU per client
- Secure interrupts (MSI/ITS)

**Strategies**

There are 3 primary approaches to virtualization support:

#. API layering (e.g. VIRTIO/VirGL. Less performance, compatible everywhere)
#. Mediated hardware access
#. Direct hardware access (using HW support for virtualization) (typically ARM, Imagination, NVIDIA, and others). This is usually the most performant solution.

:underline:`1. API layering (VIRTIO-GPU with VirGL)`

Even using general APIs like VIRTIO, rendering operations will be
executed on the host GPU and therefore requires a GPU with 3D support on the host machine.
The guest side requires additional software in order to convert OpenGL
commands to the raw graphics stack state (Gallium state) and channel them through VIRTIO-GPU to the
host. Currently the 'mesa' library is used for this purpose. The backend then receives the raw graphics stack
state and interprets it using the *virglrenderer* library from the raw state into an OpenGL form, which
can be executed as entirely normal OpenGL on the host machine. The host also translates shaders from the
TGSI format used by Gallium into the GLSL format used by OpenGL. Currently TGSI is implemented in Mesa
and supported primarily in Linux.

The API is however stable and could potentially be used by other OSes in
a different implementation. It might be difficult to require this as a
cross-platform standard for virtualization.

A recent solution called `virtio-gpu-rutabaga` provides a flexible
virtual device that is able to stream multiple graphics protocols over
it without having to define a new VirtIO device for each protocol.
Through this both Vulkan and Wayland protocols can be supported.

With a Vulkan based VIRTIO solution it become easier and more flexible
to implement support on the guest side. This is achieved by the fact
that Vulkan uses Standard Portable Intermediate Representation as an
intermediate device-independent language and the language is already a
part of the standard itself, so no additional translation between the
guest and the host are required. It is still a work in progress
[VIRTIO-VULKAN].

Details of the VirGL approach:

 - Run HW driver in a subsystem (i.e. VM)
 - Transport for API commands (e.g. OpenGL), e.g. virtio-gpu
 - Use generic drivers in client-VMs
 - Slower than hardware-provided virtualization but allows to have hardware-independent software components in the client guests.

:underline:`2. Mediated hardware access`

Mediated hardware access is as it sounds a strategy where there is some
hardware support but the Hypervisor must take a fairly large
responsibility to implement the virtualization features.

The first approach is to use the kernel's `VFIO Mediated
Device <https://docs.kernel.org/driver-api/vfio-mediated-device.html>`_
API to expose a portion of the host accessible hardware to the guest
where the hardware does not have native supported for SR-IOV.

A more recent approach builds on :ref:`VIRTIO<references>` to expose a
native context to the guest.

Both these approaches require the guest kernel to have knowledge of
the actual hardware being driven and have the appropriate driver
support.

:underline:`3. Hardware support for virtualization`

Modern automotive SoCs have special features built in to support GPU
virtualization, specifically to allow more than one system (VM) to
access the graphics capabilities while ensuring separation. Such
features shall guarantee that critical work tasks (in one VM) can use
GPU capabilities unaffected by less critical tasks (other VMs).

Typical hardware features for supporting virtualization include:

- Tagging memory accesses with an ID (VM ID or OS ID) that is used as
  part of the address when doing address translation in the MMU.
  Thus, it is guaranteed that a GPU operation belonging to a particular VM
  can only access the memory belonging to that VM.

- Handling interrupts from GPU operations:

  - Multiple separate interrupts that can be assigned to specific VMs
  - Single interrupt line – hypervisor must direct to the correct VM

- Hardware support for separate command queues/input/pipelines for GPU operations (to be used by separate VMs)
- Assignment strategies for GPU calculation cores

  - Priority based:
    This feature can interrupt the processing of a lower priority job if a
    job appears in a higher priority queue. It provides a flexible model
    that is easy to understand, but context switching between jobs may use
    some resources.

  - Partition based:
    This feature enables dedicating certain parts of the GPU calculation
    cores to specific VMs, thus guaranteeing their availability for critical
    tasks.

**Generalized design of virtualization capable GPUs**

.. _generalized: 
.. figure:: graphics_images/figure-1.png
  :align: center

  virtualization capable GPUs

The intention with the general model in is to note the similarities among
multiple hardware implementations of virtualization, even if the details
differ slightly:

There is generally one or several job queues for VMs to define the GPU
processing needs.

There is one interrupt queue per VM to notify the VMs when calculations
are completed, error conditions, etc. If the hardware cannot separate
the interrupts per VM, the Hypervisor needs to be involved in routing
the interrupt to the right VM. It is also possible that all VMs are
notified, and they need to look at their queues to know if this is a
relevant interrupt. There are security downsides to this of course.

In all solutions there is some method of allocating jobs to GPU cores
according to VM importance/priority/status.

Ultimately, these features are similar enough that initial system
planning can be done independent of hardware, and some basic level of VM
portability can be planned for over time. As is often the case, the
product requirements (number of VMs, GPU capability and performance)
still needs to be compared to the exact hardware capability, but that is
true also for non-virtualized systems. Ultimately, this specification
aims primarily to find common solutions for Hypervisors and cannot fully
address portability across hardware.

There appears however here to be an opportunity to make a formal
abstraction over these similarities, which we would like to encourage,
but we are not aware of such a fully unified API at this point.

Therefore, after this analysis the platform requirements for GPU
virtualizations are still short and there may still be room for improvement. 
Since graphical automotive systems are most likely to select a hardware
platform with virtualization support, we also conclude by requiring the
fully portable VIRTIO-GPU method as optional.

A short summary of what the Hypervisor / VP must ensure while
implementing a system with hardware

virtualization support:

- Set up the GPU to provide a set of queues with interrupts
- Pass queues to VMs through hypervisor configuration
- Pass (at least) one device interrupt to the guest
- Setup-component to run in HV (or dedicated VM)
- Configuration of queue handling policies (QoS, etc.), and/or configuration of partitioning of GPU resources (performance isolation)
- Minimum set of required policies? (E.g. should work defined by different VMs be scheduled using equal share or with different priorities?)

**AVPS Requirements:** for 3D Graphics

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-8}
    - VIRTIO-GPU is an optional feature.
  * - {AVPS-GFX-9}
    - If the hardware supports graphics virtualization (according to the common model described above) then the VP shall implement it with APIs that
      promote future portability. In particular, the full driver stack shall aim for that graphical
      software can be written as  independently as possible. In effect, each VM can then behave as if 
      it had its own dedicated GPU.

Virtualization of framebuffer / composition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Discussion**:

Virtualization approaches for framebuffer handling (for the composition
of the final display picture) may exist independently from the 3D object calculations.

These are the basic considerations:

- Need to be hardware-provided (software-based composition is too slow)
- It is generally safe to assume that modern hardware has this
- GPU and Display Composer hardware could also be used to implement composition work when there isn’t dedicated support for composition.
- It requires one (logical) composition engine per (logical) display
- Composition requirements:
  
  - At least one framebuffer for each client/VM
  - Separation of framebuffer and mask description
  - Mask description must be independently controllable by HV
  - Requirement for mask? 8-bit alpha channel? 1-bit?
  - Nested composition?
  - This would allow a guest to use a composition engine as well, e.g. video playing, overlay

**Basic design of framebuffer composition in virtualized systems:**

.. _basic: 
.. figure:: graphics_images/figure-2.png
  :align: center

  Basic framebuffer design

**AVPS Requirements:**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GFX-10}
    - The virtual platform shall support separating graphics output from VMs into different outputs, including a guarantee that VMs cannot view or modify the other VM’s output (security/safety property):
  
      * The requirement is only applicable if the hardware has support for multiple display outputs.
  * - {AVPS-GFX-11}
    - If there is a single output, it shall still be possible to assign one out of several VMs to this display output, and the guarantee that other VMs cannot read or modify the output shall still apply.
  * - {AVPS-GFX-12}
    - The virtual platform shall have the ability to compose graphics from several VMs into the final output:

      * The requirement is only applicable if the SoC has the corresponding support for hardware composition.
  * - {AVPS-GFX-13}
    - The Virtual Platform shall implement support for defining the policy for combining the graphics, including masking, considering the capabilities provided by the hardware.
  
      * The capabilities of the image combination (simple masking / alpha blending / other) shall follow the hardware capabilities.

Additional output and safe-rendering features
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Discussion**:

Some hardware platforms include additional framebuffer and display
output features.

These are often tailored towards so-called safe rendering and are
intended to guarantee that the safety-critical graphics (could be
tell-tales in cluster display) is guaranteed to be displayed correctly
(and/or it is always noticed and reported if for some reason the
graphics cannot be displayed).

These features may guarantee the graphics output by letting a checksum
follow the bitmap data and check it at the end of the pipeline as near
to the display as possible, or it may have more advanced features that
can compare actual output to the intended output with allowance for
minor detail changes.

It is strongly recommended for the Virtual Platform implementation to
support the underlying hardware features for safety, but since the
capabilities are often hardware-dependent we do not include more
detailed requirements on the virtual platform implementation.

**AVPS Requirements**:

No platform requirements at this time. Each product implementation ought
to carefully plan its own requirements in this area.
