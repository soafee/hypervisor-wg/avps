.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Storage
-------

**Discussion:**

When using hypervisor technology, data on storage devices needs to adhere
to high-level security and safety requirements, such as isolation and
access restrictions. VIRTIO and its layer for block devices provides the
infrastructure for sharing block devices and establishing isolation of
storage spaces. This is because actual device access can be controlled
by the hypervisor. However, VIRTIO favors generality over using
hardware-specific features. This is problematic in case of specific
requirements regarding robustness and endurance measures often
associated with the use of persistent data storage such as flash
devices. In this context we can spot three relevant scenarios:

- Features transparent to the guest OS.

  For these features, the required functionality can be implemented close to
  the access point, e.g., inside the actual driver. As an example, think of
  a flash device where the flash translation layer (FTL) needs to be
  provided by software. This contrasts with, for example, MMC flash devices,
  SD cards and USB thumb drives where the FTL is transparent to the
  software.

- Features established via driver extensions and workarounds at the level
  of the guest OS.

  These are features which can be differentiated at the level of (logical)
  block devices such that the guest OS uses different block devices and the
  driver running in the backend enforces a dedicated strategy for each
  (logical) block device.

E.g., guest OS and its application may require different write modes:
reliable vs. normal write.

Features that call for an extension of the VIRTIO Block device driver
standard.

:underline:`Meeting automotive persistence requirements`

A typical automotive ECU is often required to meet some unique
requirements.

It should be said that the entire “system” (in this case defined as the
limits of one ECU) needs to fulfil these and a combination of features
in the hardware, virtualization layer, operating system kernel, and
user-space applications may fulfil them together.

Typical automotive persistence requirements are:

The ability to configure some specific data items (or storage
areas/paths in file system) that are guaranteed to “immediately” get
stored in persistent memory (i.e. within a reasonable and bounded time).
Although the exact implementation might differ, it is typically considered as a
i.e. "Write Through mode" from the perspective of the user space program
in the guest, as opposed to a "Cached mode". In other words, data is
written through caches to the final persistent storage. Fulfilling this
requires analysis because systems normally use filesystem data caching
in RAM for performance reasons (and possibly inside Flash devices as
well). The key challenge with this approach is that there are well known
limits to Flash memory technology being “worn out” after a certain
number of write cycles.

:underline:`Data integrity in case of sudden power loss`

Data must not be “half-way” written or corrupted in any other sense.
This could be handled by journaling that can recognize “half-way”
written data upon next boot and roll back the data to a previous stable
state.

The challenge is that rolling back may violate some requirement that
trusts that data was “immediately stored” as described in the first
requirement. All in all, the second requirement must be evaluated in
combination with the first requirement.

Hardware that loses power cannot execute code to write data from RAM
caches to persistent memory. The implementation that balances “write
through” with “data integrity upon power loss” may differ. Some systems
can include a hardware “warning” signal that power is about to fail
(while internal capacitors in the power module might continue to provide
power for a very short time after an external power source disappears).
This could allow the system to execute emergency write-through of
critical data.

Flash lifetime (read/write cycle maximums) must be guaranteed so that
hardware does not fail. A car is often required to have a lifetime of
hardware beyond 10-15 years.

As we can see, these requirements are interrelated and can be in
conflict. They are only solved by enabling a limited use of
write-through, and simultaneously finding solutions for the other
two.

The persistent storage software stack is already complex, from the
definition of APIs that can control different data categories (Requirement 1
is only to be used for some data), storing data using a convenient
application programming interface, operating-system kernel
implementation of filesystems, and block-level storage drivers,
flash-memory controllers which in themselves (in hardware) have
several layers
implementing the actual storage. Flash memory controllers have a block
translation layer, which ensures that only valid and functioning flash
cells are being used and that automatically weeds out failing cells,
spreads the usage across cells (“wear levelling”), and implements
strategies for freeing up cells and reshuffling data into contiguous
blocks. When a virtualization layer is added, there can be another
level of complexity inserted.

Further design study is needed here, and many companies are forced to do
this work on a particular solution on a single system. There is no
single answer but there is significant common work that could get done.
We would encourage the industry to continue this discussion and to
develop common design principles through collaboration on
implementation, analysis methods and tooling, and then to discuss how
standards may ensure the compatibility of these solutions.

Block Devices
~~~~~~~~~~~~~

**Discussion:**

While the conclusion from the introduction remains, that a particular
system must be analyzed and designed to meet its specific requirements,
the working group concluded that for the AVPS purpose, VIRTIO block
device standards could be sufficient in combination with the right
requests being made from user space programs (and running appropriate
hardware devices below).

With VIRTIO the options available for write cache usage are as below:

Option 1: WCE = on, i.e. device can be set in write-through mode, in
VIRTIO block device.

Option 2: WCE = off, i.e. the driver follows BLK_SYNC after BLK_WRITE.
The open device call with O_SYNC from user space in Linux ensures fsync
after the write operation. The file-system mount can also enable
synchronous file operation and it is available through O-option (and
only some guarantee to respect it 100%). The Linux API creates a block
request with FUA (Forced Unit Access) flag for ensuring that the
operation is performed to persistent storage and not through volatile
cache.

The conclusion is that VIRTIO does not break the native behavior. Even
in the native case write cache can be somewhat uncertain but VIRTIO does
not make it worse.

VIRTIO does not provide Total Blocks Written or other parts of
S.M.A.R.T. information from the physical disks. We agree that omitting
access to host-related information such as disk health data from the
virtual platform API is appropriate, because giving virtual machines
access to this might have subtle security related effects. Imagine for
example the ability for a malicious program to probe for behaviors that
cause declining disk health and then exploit those behaviors, or
consider generally the implication of VMs analyzing what other VMs are
doing.

.. note:: UFS devices provide LUN configuration (Logical Unit Number) also called
  as UFS provisioning support such that the devices can have more than one
  LUNs. A FUA to one LUN does not mean all caches will be flushed. eMMC
  does not provide such support. SCSI devices like HDD provide the support
  for multiple LUNs. Similarly, PCIe NVMe SSD can be configured to have
  more than one namespaces. One could map partitions onto LUNs/namespaces
  (some optimized for write-through and some for better average
  performance) and build from there.

**AVPS Requirements:**

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-STOR-1}
    - The platform MUST implement virtual block devices
      according to Virtio Specification [VIRTIO].
  * - {AVPS-STOR-2}
    - The platform MUST implement support for the
      VIRTIO_BLK_F_FLUSH feature flag.
  * - {AVPS-STOR-3}
    - The platform MUST implement support for the
      VIRTIO_BLK_F_CONFIG_WCE feature flag.
  * - {AVPS-STOR-4}
    - The platform MUST implement support for the
      VIRTIO_BLK_F_DISCARD feature flag.
  * - {AVPS-STOR-5}
    - The platform MUST implement support for the VIRTIO_BLK_F_WRITE_ZEROS feature flag.
