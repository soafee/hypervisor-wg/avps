.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Media codecs
------------

**Discussion:**

This chapter covers hardware support devices for encoding and decoding
of compressed media formats (audio and video) and the potential of VMs
to share these hardware capabilities.

Typically there are two interface types defined for multimedia codecs. A
stateful interface does not require the user to maintain additional
information, like the amount of the encoded data to be processed at the
next step and corresponding format dependent metadata, to perform buffer
processing, it is done internally by the hardware and by the driver. A
stateless interface in turn implies that on each processing step the
user is expected to maintain the current state of operation and provide
it to the device on a per frame basis to advance the processing
pipeline.

For example, for the decoder use case the stateless interface requires a
lot of stream processing steps (like metadata parsing) to be done by the
user in software. This means, in case the actual hardware is stateful,
there would be not enough data to perform any real operation on the
multimedia stream on the hypervisor side. On the other hand, if the
paravirtualized multimedia device implements the stateful interface, it
should not be a problem to handle the data and do any required parsing
on the hypervisor side if the real hardware has either stateful or
stateless interface.

Therefore the virtual device interface should be stateful to be possible
to implement on all hardware variants.

A number of different proposals have been made to support media codecs
although no consensus has yet been reached for ratification. As the
problem space could be quite wide it is conceivable a number of
different approaches may be adopted depending on the exact
requirements.

The virtio-video proposal (see `v8
<https://lists.oasis-open.org/archives/virtio-comment/202306/msg00549.html>`_)
proposes a simple stateful interface device that maps fairly simply
onto the hosts presentation of a `V4L2 device
API <https://www.kernel.org/doc/html/v4.9/media/uapi/v4l/v4l2.html>`_.
It defines a command-response set to report and/or negotiate
capabilities, stream formats, and various codec-specific settings.

The specification draft defines encoder and decoder capabilities. The
actual codec hardware performs the encoding/decoding operation. The
virtual platform provides concurrent or arbitration between multiple
guests. The hypervisor can also enforce some resource constraints in
order to better share the capabilities between VMs.

The virtio-media proposal (see `virtio-media
<https://github.com/Gnurou/virtio-media>`_) instead suggests a direct
mapping of the V4L2 API to the VirtIO specification. It is not being
proposed for formal specification although will likely be the solution
adopted by ChromiumOS going forward.

**AVPS Requirements**:

[VIRTIO-video] is designed to define a virtual interface to video
encoding and decoding devices.

Since the VIRTIO-video proposal is not ratified yet, no requirements are
defined at this time but it is likely in a later release of the
document.

.. list-table:: 
  :widths: 20 80
  
  * - [PENDING]
    - If a system requires video codec sharing, it MUST be
      implemented according to the VIRTIO-video requirements specified in [VIRTIO-X.X]

.. todo::  Assign and finalize the existing requirement.
  Potential for future work exists here.
