.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
General system
--------------

Booting guest virtual machines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Discussion:**

A boot protocol is an agreed way to boot an operating system kernel on
hardware or virtual hardware. It passes on information to operating systems (OS) about
the available hardware, usually by providing a device tree
description. Certain hardware specifics are provided in an
abstract/independent way by the hypervisor (or, typically, by the
initial boot loader on real hardware). These can be base services like
real-time clock, wake-up reason, enabling cores, and so on.

Outside of the PC world, boot details have traditionally been very
hardware/platform specific.

The Embedded Base Boot Requirements (EBBR) specification from ARM |(r)|
has provided a reasonable outline for a boot protocol that can also be
implemented on a virtual platform. The EBBR specification defines the
use of UEFI APIs as the way to do this.

A small subset of UEFI APIs is sufficient for this task and it is not
too difficult to handle. Some systems running in VMs do not implement
the EBBR protocol (e.g. ported legacy AUTOSAR |(r)| Classic based systems and
other RTOS guests). These are typically implemented using a compile-time
definition of the hardware platform. It is therefore expected that some
of the code needs to be adjusted when porting such systems to a virtual
platform. However, requiring EBBR is a stable basis for operating
systems that can use it.

We expect two categories of boot information handling:

1) Static setup: The hypervisor exposes the devices at statically
defined addresses. This allows the system integrator to incorporate the
relevant devices into the guest at compile time and configure the
hypervisor indirectly. Thus, an explicit exposed device configuration
provides a consistent basis between the compile time configuration of
the guest and the environment exposed to the guest by the hypervisor at
runtime.

This is beneficial when dealing with specialized, small footprint OS
which commonly do not process platform specific information at boot-time
or when using manual or fully automatized SW configuration frameworks as
common for pre-compile configuration of AUTOSAR |(r)| SW stacks.

2) Dynamic setup: The hypervisor decides where to place devices and
communicates that to the guest operating system at guest boot-time. Once
again, this can be done by device-tree dynamically generated and exposed
to the guest OS. Dynamic handling of boot information is more flexible
as it does not require a potentially err-prone re-configuration of the
guest. In turn, the guest needs to be able to process this information
at boot-time which is often only found with general-purpose OS such as
Linux.

In both cases the specification of platform layout benefits from
deciding on a firm standard for how the Hypervisor describes a
hardware/memory map to legacy system guests. The consensus seems to be
that Device Tree is the most popular and appropriate way, and an
independent specification is available as Device tree descriptions are
also a human readable specification and can be directly incorporated
into any kind of system architecture documentation.

The group found that Android and typical GNU/Linux style systems often
have different boot requirements. However, Android could be booted using
UEFI and it is therefore assumed that the EBBR requirements can be
applicable for running guests.

**AVPS Requirements:**


.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-GEN-1}
    - Virtual platforms that support a dynamic boot protocol MUST implement (the mandatory parts of*) EBBR.
  * - {AVPS-GEN-2}
    - Since EBBR allows either ACPI or Device Tree implementation, this
      can be chosen according to what fits best for the chosen hardware
      architecture and situation.
  * - {AVPS-GEN-3}
    - For systems that do not support a dynamic boot protocol (see
      discussion), the virtual hardware SHOULD still be described using a
      device tree format, so that guest-VM and hypervisor implementers
      can agree on the implementation using that formal description.
