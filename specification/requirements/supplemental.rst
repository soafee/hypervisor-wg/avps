.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Supplemental Virtual Device categories
--------------------------------------

.. todo:: The specification and this chapter talks a lot of VIRTIO devices but does say anything about the transport. We need to add a chapter on VIRTIO transport.

Text Console
^^^^^^^^^^^^

**Discussion:**

While they may be rarely an appropriate interface for the normal
operation of the automotive system, text consoles are expected to be
present for development purposes. The virtual interface of the console
is adequately defined by [VIRTIO].

Text consoles are often connected to a shell capable of running
commands. For security reasons, text consoles need to be possible to
shut off entirely in the configuration of a production system.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-CON-1}
    - The virtual interface of the console MUST be
      implemented according to chapter 5.3 in [VIRTIO]
  * - {AVPS-CON-2}
    - To not impede efficient development, text consoles
      shall further be integrated according to the operating systems'
      normal standards so that they can be connected to any normal
      development flow.
  * - {AVPS-CON-3}
    - For security reasons, text consoles MUST be possible
      to shut off entirely in the configuration of a production system.
      This configuration MUST NOT be modifiable from within any guest
      operating system.
  * - {AVPS-CON-4}
    - It is also recommended that technical and/or process related
      countermeasures are introduced and documented during the
      development phase, to ensure there is no way to forget to disable
      these consoles.

Filesystem virtualization
^^^^^^^^^^^^^^^^^^^^^^^^^

**Discussion**:

This chapter discusses two different features, one being host-to-VM
filesystem sharing and the other being VM-to-VM sharing, which might be
facilitated by hypervisor functionality.

The function of providing disk access in the form of a "shared folder"
or full disk pass-through is a function that seems more used for
desktop virtualization than in the embedded systems that this document
is for. In desktop virtualization, for example the user wants to run
Microsoft Windows in combination with a MacOS host, or to run Linux in
a virtual machine on a Windows-based corporate workstation, or to try
out custom Linux systems in KVM/QEMU on a Linux host, for development
of new (e.g. embedded) systems.

Host-to-VM filesystem sharing are also useful when sharing immutable
file-systems with multiple guest VMs. For example when sharing a
number of file-system layers for a container runtime there is an
opportunity for the host system to reduce duplication by sharing the same
RO page with multiple domains.

The working group found little need for this host-to-vm disk sharing in
the final product in most automotive systems, but we summarize the
opportunities here if the need arises for some product.

:ref:`VIRTIO<references>` mentions, very briefly, one network disk
protocol for the purpose of hypervisor-to-vm storage sharing, which is
9pfs. 9pfs is a part of a set of protocols defined by the legacy Plan9
operating system. While support for 9pfs (see :ref:`9PFS<references>`)
exists in QEMU for a number of transports (Xen and VirtIO) it has not
been formally specified.

A more advanced network disk protocol such as NFS, SMB/SAMBA would be
too heavy to implement in the HV-VM boundary, but larger guest systems
(like a full Linux system) can implement them within the normal
operating system environment that is running in the VM. Thus, the
combined system could likely use this to share storage between VMs over
the (virtual) network and in that case the hypervisor/virtual platform
does not need an explicit implementation.

The 1.2 VirtIO specification introduced a new File System Device (see
:ref:`VIRTIO-FS<references>`) which aims to “provide local file system
semantics between multiple virtual machines sharing a directory tree”.
It uses the FUSE protocol over VIRTIO, which means reusing a proven
and stable interface and guarantees the expected POSIX filesystem
semantics also when multiple VMs operate on the same file system.
Together with support for shared memory there is an opportunity for
the host to share data cached in memory between multiple VMs.

As stated, it is uncertain if fundamental host-to-vm file system sharing
is a needed feature in typical automotive end products, but the new
capabilities might open up a desire to use this to solve use-cases that
were previously not considering shared filesystem as the mechanism. We
can envision something like software update use-cases that have the
Hypervisor in charge of modifying the actual storage areas for code. For
this use case, download of software might still happen in a VM which has
advanced capabilities for networking and other operations, but once the
data is shared with the HV, it could take over the responsibility to
check software authenticity (after locking VM access to the data of
course) and performing the actual update.

In the end, using filesystem sharing is an open design choice since the
data exchange between VM and HV could alternatively be handled by a
different dedicated protocol.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-FS-5}
    - If filesystem virtualization is implemented, then
      :ref:`VIRTIO-FS<references>` MUST be one of the supported
      choices.
