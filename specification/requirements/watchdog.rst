.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Watchdog
--------

**Discussion:**

A watchdog is a device that supervises that a system is running by using
a counter that periodically needs to be reset by software. If the
software fails to reset the counter, the watchdog assumes that the
system is not working anymore and takes measures to restore system
functionality, e.g., by rebooting the system. Watchdogs are a crucial
part of safety-concerned systems as they detect misbehavior and stop a
possibly harming system.

In a virtualized environment, the hypervisor shall be able to supervise
that the guest works as expected. By providing a VM a virtual watchdog
device, the hypervisor can observe whether the guest regularly updates
its watchdog device, and if the guest fails to update its watchdog, the
hypervisor can take appropriate measures to ensure a possible
misbehavior and to restore proper service, e.g., by restarting the VM.

While a hypervisor might have non-cooperating means to supervise a
guest, being in full control over it, using a watchdog is a
straight-forward and easy way to implement a supervision functionality.
An implementation is split in two parts, one being the in the
hypervisor, the device, and another in the guest operating system, a
driver for the device offered by the hypervisor. As modifying and adding
additional drivers to an operating system might be troublesome because
of the effort required, it is desirable to use a watchdog driver that is
already available in guest operating systems.

Fortunately, there are standard devices also for watchdogs. The Server
Base System Architecture [SBSA] published by ARM defines a generic
watchdog for ARM systems, which also has a driver available in the
popular Linux kernel and thus only requires hypervisors to provide a
virtual generic watchdog device according to SBSA's definition (device
compatible: "arm,sbsa-gwdt"). The specification offers appropriate
actions in case the guest fails to update the watchdog.

We therefore recommend hypervisors to implement the watchdog according
to the generic watchdog described in SBSA, not only on ARM systems but
regardless of the hardware architecture used in the system.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-WDG-1}
    - The platform MUST implement a virtual hardware
      interface to the hardware watchdog, following the generic watchdog
      described in Server Base System Architecture 6.0 [SBSA]
