.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Communication Networks
----------------------

Standard networks
~~~~~~~~~~~~~~~~~

**Discussion**:

Standard networks include those that are not automotive specific and not
dedicated for a special purpose like Automotive Audio Bus(R) (A2B).
Instead these are frequently used in the computing world, and for our
purposes nowadays that means almost always within the Ethernet family
(802.xx standards).

These are typically IP based networks, but some of them simulate this
level through other means (e.g. vsock, which does not use IP
addressing). The physical layer is normally some variation of the
Ethernet/Wi-Fi standard(s) (according to standards 802.*) or other
transport that transparently exposes a similar network socket
interface. Certain alternative networks will provide a channel with
Ethernet-like
capabilities within them (APIX, MOST, ...) but those are
automotive-specific. These might be called out specifically where
necessary, or just assumed to be exposed as standard Ethernet network
interfaces to the rest of the system.

Some existing virtualization standards to consider are:

- VIRTIO-net = Layer 2 (Ethernet / MAC addresses)
- VIRTIO-vsock = Layer 4. Has its own socket type. Optimized by stripping away the IP stack. 
  Possibility to address VMs without using IP addresses. 
  Primary function is Host (HV) to VM communication.

Virtual network interfaces ought to be exposed to user space code in the
guest OS as standard network interfaces. This minimizes custom code
appearing because the usage of virtualization is minimized.

MTU (Maximum Transmission Unit) may differ depending on the actual network being used. There is a
feature flag that a network device can state its maximum (advised) MTU
and the guest application code might make use of this to avoid segmented
messages.

The guest may require a custom MAC address on a network interface. This
is important for example when setting up bridge devices which expose the
guest's MAC address to the outside network.

To avoid clashes the hypervisor must be able to set an explicit (stable
across reboots) MAC address in each VM.

In addition, the guest shall be able to set its own MAC address,
although the HV may be set up to deny this request for security reasons.

Offloading and similar features are considered optimizations and
therefore not absolutely required.

The virtual platform ought to provide virtual network interfaces using
the operating system’s normal interface concept (i.e. they should show
up as a network device) but the exact details of that may depend on the
operating system run in the VM, if the virtual platform includes
paravirtualization, which is very
likely. This ought therefore not be written as a requirement in the
kernel-to-hypervisor API, but it shall be considered when providing a
platform solution.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-NET-1#1}
    - If the platform implements virtual networking, it MUST
      use the VIRTIO-net.
  * - {AVPS-NET-2#2}
    - The hypervisor MUST provide the ability to
      dedicate and expose any physical network interface to a virtual
      machine (pass-through).
  * - {AVPS-NET-3}
    - REMOVED.
  * - {AVPS-NET-4#1}
    - Implementations of VIRTIO-net MUST support the VIRTIO_NET_F_MTU feature.
  * - {AVPS-NET-5#1}
    - Implementations of VIRTIO-net MUST support the VIRTIO_NET_F_MAC feature.
  * - {AVPS-NET-6#1}
    - Implementations of VIRTIO-net MUST support the VIRTIO_NET_F_CTRL_MAC_ADDR feature.
  * - {AVPS-NET-7}
    - The Hypervisor MAY implement a whitelist or other way
      to limit the ability to change MAC address from the VM.

VSock and inter-VM networking
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Discussion:**

.. todo:: Improve this section, also introducing and describing inter-VM
   network switching. VSock is often understood as a HV <-> VM communication
   channel.

VSock is a “virtual” (artificial) socket type in the sense that it does
not implement all the layers of a full network stack that would be
typical of something running on Ethernet, but instead provides a simpler
implementation of network communication directly between virtual
machines (or between VMs and the Hypervisor). The idea is to shortcut
anything that is unnecessary in this local communication case while
still providing the socket abstraction. Higher level network protocols
should be possible to implement without change.

When using the VSock (VIRTIO-vsock) standard, each VM has a logical ID
but the VM normally does not know about it. Example usage: Running an
agent in the VM that does something on behalf of the HV.

For the user-space programs the usage of vsock is very close to
transparent, but programs still need to open the special socket type
(AF_VSOCK). In other words, it involves writing some code that is custom
for the virtualization case, as opposed to native, and we recommend
system designers to consider this with caution for maximum portability.

Whereas vsock defines the application API, multiple different named
transport variations exist in different hypervisors, which means the
driver implementation differs depending on chosen hypervisor.
VIRTIO-vsock however locks this down to one chosen method.

**AVPS Requirements:**


.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-NET-8}
    - The virtual platform MUST be able to configure
      virtual inter-VM
      networking interfaces (either through VSOCK or providing other
      virtual network interfaces that can be bridged)
  * - {AVPS-NET-9}
    - If the platform implements VSOCK, it MUST use the
      VIRTIO-vsock required API between drivers and Hypervisor.

Wi-Fi
~~~~~

**Discussion:**

Wi-Fi adds some additional characteristics not used in wired networks:
Network IDs (SSID), password and authentication information, signal
strengths, preferred frequencies, and so on.

There are many potential systems designs possible and no single way
forward for virtualizing Wi-Fi hardware. More discussion is needed to
converge on the most typical designs, as well as the capability (for
concurrent usage) of typical Wi-Fi hardware. Together this may determine
how much it would be worth to create a standard for virtualized Wi-Fi
hardware.

Examples of system designs could include:

- Exposing Wi-Fi to only one VM and let that act as an explicit
  gateway/router for the other VMs.

- Let the Wi-Fi interface be shared on the Ethernet level, similar to how
  other networks can be set up to be bridged in the HV. In this case some
  of the network setup such as connecting to an access point, handling of
  SSID and authentication would need to be done by the Hypervisor, or at
  least one part of the system (e.g. delegate this task to a specific VM).

- Virtualizing the Wi-Fi hardware, possibly using capabilities in some
  Wi-Fi hardware that allow connecting to multiple access points at the
  same time.

To do true sharing it would be useful to have a Wi-Fi controller that
can connect to more than one endpoint (Broadcom, Qualcomm, and others,
reportedly have such hardware solutions.)

A related proposal is MAC-VTAB to control pass-through of the MAC
address from host to VM. Ref:

**AVPS Requirements:**

.. todo:: This chapter sets no requirements currently since the capability of typical Wi-Fi hardware, the preferred system designs, and defining standards for a virtual platform interface needs more investigation.
  
  Potential for future work exists here.

Time-sensitive Networking (TSN)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Discussion:**

TSN adds the ability for ethernet networks to handle time-sensitive
communication including reserving guaranteed bandwidth, evaluating
maximum latency through a network of switches, and adding fine-grained
timestamps to network packets. It is a refinement of the previous
Audio-Video Bridging (AVB) standards, in order to serve other
time-sensitive networking.

It is not yet clear to us how TSN affects networking standards. Many
parts are implemented at a very low level, such as time-stamping packets
being done in some parts of the Ethernet hardware itself to achieve the
necessary precision. For those parts it might be reasonable to believe
that nothing changes in the
usage, compared to non-virtual platforms, or that little need to change
in the Virtual Platform API definitions compared to standard networks.

Other parts are however on a higher protocol level, such as negotiating
guaranteed bandwidth, and other monitoring and negotiation protocols.
These may or may not be affected by a virtual design and further study
is needed.

A future specification may include that the hypervisor shall provide a
virtual ethernet switch and implement the TSN negotiation protocols, as
well as the virtual low-level mechanisms (e.g. Qbv Gate Control Lists).
This requirement would be necessary only if TSN features are to be used
from the VM.

**AVPS Requirements**:

.. todo:: To be added in a later version.

  Potential for future work exists here.
