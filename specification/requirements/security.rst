.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Cryptography and Security Features
----------------------------------

**Sharing of crypto accelerators.**

On ARM, crypto accelerator hardware is often only accessible from
TrustZone, and stateful as opposed to stateless. Both things make
sharing difficult.

A cryptography device exists in VIRTIO (intended to model crypto
accelerator for ciphers, hashes, MACs, AEAD) [VIRTIO-CRYPTO]

**RNG and entropy**

VIRTIO-entropy (called virtio-rng inside Linux implementation) is
preferred because it is a simple and cross-platform interface.

Some hardware implements only one RNG in the system and it is in
TrustZone. It is inconvenient to call APIs into TrustZone in order to
get a value that could just be read from the hardware but on those
platforms, it is the only choice. While it would be possible to
implement VIRTIO-entropy via this workaround, it is more convenient to
make direct calls to TrustZone.

The virtual platform is generally expected to provide access to a
hardware-assisted high-quality random number generator through the
operating system's preferred interface (/dev/random device on Linux)

The virtual platform implementation should describe a security analysis
of how to avoid any type of side-band analysis of the random number
generation.

Random Number Generation
^^^^^^^^^^^^^^^^^^^^^^^^

**Discussion:**

Random number generation is typically created by a combination of a
true-random and pseudo-random implementations. A pseudo-random
generation algorithm is implemented in software. "True" random values
may be acquired by an external hardware device, or a built-in hardware
(noise) device may be used to acquire a random seed which is then
further used by a pseudo-random algorithm. VIRTIO specifies an entropy
device usable from the guest to acquire random numbers.

In order to support randomization of physical memory layout (as Linux
does) the kernel also needs a good quality random value very early in
the boot process, before any VIRTIO implementations can be running. The
device tree describes the location to find this random value or
specifies the value itself. The kernel uses this as a seed for
pseudo-random calculations that decide on the virtual memory layout.

Traditionally a requirement for a “true” random generator is required
but there is a lot of debate of whether this truly improves the
situation compared to pseudo-random generators. In particular, it is
harmful if too many processes “deplete” the true random generator to the
extent that PRNGs cannot be reliably seeded, and applications may then
effectively receive statistically less random numbers.

**AVPS Requirements**:

To support high-quality random numbers to the kernel and user-space
programs:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-SEC-1}
    - The Virtual Platform MUST offer at least one good
      entropy source accessible from the guest.
  * - {AVPS-SEC-2}
    - The entropy source SHOULD be implemented according to
      VIRTIO Entropy device, chapter 5.4 [VIRTIO]

      | To be specific, it is required that what h received from the
        implementation of the VIRTIO entropy device SHOULD contain only
        entropy.

To support memory layout randomization in operating systems that support
it (Linux specifically):

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-SEC-3}
    - The virtual platform MUST provide a high-quality
      random number seed immediately available during the boot process
      and described in the hardware device tree using the name kaslr-seed
      :ref:`[DeviceTree-Chosen]<references>`

Trusted Execution Environments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Discussion**:

Access to TrustZone and equivalent Trusted Execution Environments (TEE)
is a feature that is frequently requested from the guest, so when legacy
systems are ported from native hardware to a virtual platform, should
not require significant modification of the software. Accessing the
trusted execution environment should work in the exact same way as for a
native system. This means it can be accessed using the standard access
methods that are typically involved executing a privileged CPU
instruction (e.g. SMC calls on ARM, equivalent calls on other architectures). Another option
used on some x86-based systems is to run OPTEE instances, one per guest. The
rationale for this is that implementations that have been carefully
crafted for security (e.g.
Multimedia DRM) are unlikely to be rewritten only to support
virtualization.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-SEC-4#1}
    - Access to TrustZone and equivalent functions MUST work
      in the exact same way as for a native system using the standard
      access methods (SMC calls on ARM, equivalent mechanisms on other architectures).

Replay Protected Memory Block (RPMB)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Discussion:**

The Replay Protected Memory Block (RPMB) provides a means for the system
to store data to a specific memory area in an authenticated and replay
protected manner. An authentication key information is first programmed
to the device. The authentication key is used to sign the read and write
made to the replay protected memory area with a Message Authentication
Code (MAC). The feature is provided by several storage devices like
eMMC, UFS, NVMe SSD, by having for one or more RPMB area.

Different guests need their own unique Strictly Monotonic Counters. It
is not expected for counters to increase by more than one, which could
happen if more than one guest shares the same mechanism. The RPMB key
must not be shared with multiple guests and another concern is that RPMB
devices may define maximum write block sizes, so it would require
multiple writes if the data chunk were large, making the process no
longer atomic from one VM. Implementing a secure setup for this is for
now beyond the scope of this description, but it seems to require
establishing a trusted entity that implements an access “server”, which
in turn accesses the shared RPMB partition.

Notably, most hardware includes two independent RPMB slots, which
enables at least two VMs to use the functionality without implementing
the complexities of sharing.

Some platforms offer virtualized RPMB support whereas some platforms
instead use hardware passthrough to offer RPMB functionality to the TEE.

The specification for :ref:`VIRTIO-RPMB<references>` was included in
VIRTIO version 1.2 and some example backends exits for it. However
adoption has been slowed due to the need to implement a common Linux
API for RPMB (where existing RPMB implementations are tied to the
underlying storage device nodes).

.. todo:: Assess if the current specification addresses all the
          implementation complexity, or leaves the solution open as
          unknown implementation details.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-SEC-5}
    - If the platform provides virtualized replay
      protection, the device MUST be implemented according to the RPMB
      requirements specified in :ref:`VIRTIO-RPMB<references>`

Crypto acceleration
^^^^^^^^^^^^^^^^^^^

**Discussion:**

VIRTIO-crypto standard seems to have been started primarily for PCI
extension cards implementing crypto acceleration, although specification
seems generic enough to support future (SoC) embedded hardware.

The purpose of acceleration can be pure acceleration (client has the
key) or rather HSM purpose (such as key is hidden within hardware).

The implementation consideration is analogous to the discussion on RNGs.
On some ARM hardware these are offered only within TrustZone and in
addition the hardware implementation is stateful. It ought to be
possible to implement VIRTIO-crypto also by delegation into TrustZone
and therefore we require it also on such platforms however it should be
understood that parallel access to this feature may not be possible,
meaning that this device can be occupied when a guest requests it. This
must be considered in the complete system design.

**AVPS Requirements**:

   .. list-table:: 
    :widths: 20 80
  
    * - {AVPS-SEC-6}
      - If the virtual platform implements crypto
        acceleration, then the virtual platform MAY implement VIRTIO-crypto
        as specified in chapter 5.9 in [VIRTIO]. (ref: [VIRTIO-CRYPTO])
        
        .. note:: This requirement might be a MUST later, if the hardware is appropriate, and optional for hardware platform platforms that are limited to single-threaded usage or other limitations. At that point a more exact list of required feature bits from VIRTIO should be specified.
