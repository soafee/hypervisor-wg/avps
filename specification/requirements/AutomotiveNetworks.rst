.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Automotive networks
-------------------

This chapter covers some traditional in-car networks and buses, such as
CAN, FlexRay, LIN, MOST, etc., which are not Ethernet TCP/IP style
networks treated in the Standard Networks chapter.

CAN
^^^
**Discussion:**

The AVPS working group has found and discussed some work related to
virtualizing CAN. Virtio-CAN will be included in the virtio-1.4
specification (see `git commit <https://github.com/oasis-tcs/virtio-spec/commit/07bb9f7f642df2ea9ef93ac3e834077038020a1d>`_ of the specification).

Like many automotive networks, it seems likely that a system may
separate the responsibility for this communication into either a
dedicated separate core in a multi-core SoC, or to a single VM, and then
forward the associated data to/from other VMs from that single point.

CAN might be worth special consideration due to that some virtualization
work has been presented.

.. todo:: 2021/April: A formal VIRTIO-CAN proposal was sent to the VIRTIO development mailing list, which is worth tracking for the future.

**AVPS Requirements**:

We do not specify any requirements at this time since there is no
obviously well adopted standard, nothing has been accepted into upstream
specifications, and we have not yet had enough stakeholders to agree
that the AVPS should put forward a single defined standard.

However, this requirement may be updated if VIRTIO-CAN gets traction.

.. todo:: Potential for future work exists here.

Local Interconnect Network (LIN)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Discussion:**

LIN is a serial protocol implemented on standard UART hardware. For that
reason, we assume that the standard way to handle serial hardware in
virtualization is adequate.

Like many automotive networks, it also seems likely that a system may
separate the responsibility for this communication into either a
dedicated separate core in a multi-core SoC, or to a single VM, and then
forward the associated data to/from other VMs from that single point.

Special consideration for virtualizing the LIN bus may therefore seem
unnecessary, or not worth the effort now.

Reports of design proposals or practical use of LIN in a virtualized
environment are welcome to refine this chapter.


**AVPS Requirements**:

.. todo:: Refer to any requirements given on serial devices.

FlexRay
^^^^^^^

**Discussion:**

FlexRay |(tm)| has not been studied in this working group.

Like many automotive networks, it seems likely that a system may
separate the responsibility for this communication into either a
dedicated separate core in a multi-core SoC, or to a single VM, and then
forward the associated data to/from other VMs from that single point.

Device virtualization for the FlexRay bus itself may therefore seem
unnecessary, or not worth the effort now.

Reports of design proposals or practical use of FlexRay in a virtualized
environment are welcome, in order to refine this chapter.

**AVPS Requirements**:
None at this time.

CAN-XL
^^^^^^

**Discussion:**

CAN-XL is still in development. We welcome a discussion with the
designers on how or if virtualization design should play a part in
this, and how the Automotive Virtual Platform definition can support
it.

**AVPS Requirements**:
None at this time.

MOST
^^^^

**Discussion:**
Media Oriented Systems Transport (MOST) has not been studied by the
AVPS working group.

Reports of design proposals or practical use of MOST in a virtualized
environment are welcome, in order to refine this chapter.

**AVPS Requirements**:

None at this time.
