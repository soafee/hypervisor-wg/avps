.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../../macros.rst
Power and System Management
---------------------------

**Discussion**:

Power and system management can be an important part of the system, and
takes care of management of the virtual platform as well as passing
information to the hypervisor if the VM accesses devices directly.

Tasks include:

- Shutdown and reset of the virtual platform
- Enabling and disabling of cores for multi-core VMs
- Informing the hypervisor on performance requirements
- Suspend-to-RAM of the virtual platform
- Inform the hypervisor on secondary/indirect peripheral use, such
  as peripheral clocks and peripheral power-domains

Since the hypervisor is responsible to keep the overall system in a
secure and safe state, it must offer an arbitration service that is able
to take decisions:

- Deny or accept a guest requests depending on the status the system
  in general and of other guests.
- Intercept power management events
  (power failures, user interactions, etc.) and forward them to the
  relevant guests safely and in the correct order.

To facilitate portability of VMs between hardware platforms, the VMs
shall use a platform-independent API as much as possible.

On Arm systems, there exist standardized interfaces for platform and
power management topics:

- They offer interfaces for suspend/resume, enabling/disabling cores,
  secondary core boot, system reset and power-down as well as core
  affinity and status information.

- They offer a set of operating-system independent interfaces for system
  management, including power domain management, performance management of
  system components, clock management, sensor management and reset domain
  management.

The interfaces are built in a way allowing a hypervisor to implement
those interfaces for guests, and possibly arbitrating and managing
requests for multiple guests.

When a hypervisor offers features regarding power and system management
to virtualization guests on the Arm platform, the hypervisor shall offer
virtual PSCI and SCMI interfaces to the guest.

For other architectures, the landscape is diverse, such that this
specification can only recommend using the standard mechanisms used on
those architectures and implement appropriate support in the hypervisor
environment to support guests.

For x86-based systems, it is typical for the virtual platform to
expose a virtual interface that follows ACPI.

There are similar concepts in ACPI, PSCI and SCMI but creating a unified
interface is not realistic at this point. Therefore the approach in this
specification is to provide different requirements for different
hardware platforms. So far, the specification has firm requirements on
Arm-based systems only, and more analysis would be needed to write firm
requirements for x86-64 (Intel/AMD), RISC-V, MIPS, PowerPC, SH (e.g.
V850 microcontrollers) and other architectures.

A definition of SCMI over VIRTIO was recently merged into the master
branch of the specification development, see [VIRTIO-SCMI]. Future
requirements are likely to reference this specification as required
support.

**AVPS Requirements**:

.. list-table:: 
  :widths: 20 80
  
  * - {AVPS-POW-1}
    - AVPS requires that compatible hypervisors on the Arm
      architecture implement functionality using PSCI and SCMI (ref:
      [SCMI]) when such a feature is offered by the hypervisor.
      If these features are included, they shall fulfil the specifications
      as written:
   
      * Required:
       
        - VM-Core on/off: Arm: PSCI
        - VM-reset, VM-poweroff: Arm: PSCI
        - Idle/sleeping states: Arm: PSCI
        - Suspend-to-ram: Arm: PSCI

      * Optional:

        - CPU Performance Management: Arm: SCMI Clock, Power domains,
          Performance domains, Reset domains: Arm: SCMI

      Required PSCI Interface: v1.1

      Required SCMI Interface: v2.0 or later

.. todo:: Potential for future work exists (for additional CPU architectures) here.
