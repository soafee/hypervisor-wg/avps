.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../macros.rst
============
Introduction
============

This specification covers a collection of virtual device driver APIs and other requirements. The APIs constitute the defined interface between virtual machines and the virtualization layer, i.e. the hypervisor or virtualization "host system". Together, the APIs and related requirements define a virtual platform. This specification, the Automotive Virtual Platform Specification (AVPS), describes the virtual platform such that multiple implementations can be made, compatible with this specification.

Initially, a working group within the Hypervisor Project, within the GENIVI Alliance, prepared this specification, and a good deal of information was provided by sources outside the automotive industry.
GENIVI, now COVESA, develops standard approaches for integrating operating systems and middleware in automotive systems and promotes a common vehicle data model and standard service catalog for use in-vehicle and in the vehicle cloud.

With GENIVI refocusing, and renaming itself to COVESA, the hypervisor working group was dissolved and the AVPS release 2.0 was the last release by the COVESA's hypervisor working group.

Some time later, a group of hypervisor and virtualization enthusiasts formed a Hypervisor working group under the umbrella of SOAFEE. The group's focus is to work with SOAFEE members and others on defining and building virtualization into the open source reference stack developed by SOAFEE.
Part of this is defining interoperability between different parts of the overall system architecture of an ECU in a software defined vehicle.  After a formal hand-over from COVESA, the AVPS found a new home in the Hypervisor working group of SOAFEE, contributing to the goal of defining interoperability across virtualization solutions.

The specification remains open licensed and contributions to the specification are very welcome.

Automotive systems use software stacks with particular needs. Existing standards for virtualization from other areas sometimes need to be augmented, partly because their original design was not based on automotive or embedded systems. The industry needs a common initiative to define the basics of virtualization as it pertains to Automotive whereas much of the progress in virtualization has come from IT/server consolidation and a smaller part from the virtualization of workstation/desktop systems. Virtualization for embedded systems is still at an early stage, but its use is increasing. Support for virtualization in the embedded space is starting to appear in the upstream project initiatives, such as VIRTIO, which this specification heavily relies upon.

A shared virtual platform definition in automotive creates many advantages:

- It simplifies moving hypervisor guests between different hypervisor environments.
- It can over time simplify reuse of existing legacy systems in new, virtualized, setups.
- Device drivers in operating system kernels (e.g., Linux), targetting hypervisor interfaces, do not require separate maintenance for each hypervisor.
- There is potential for shared implementation across guest operating systems.
- There is potential for shared implementation across hypervisors with different license models.

A specification can enable industry shared requirements and test suites, a common vocabulary and understanding to reduce the management complexity of systems using virtualization.

As a comparison, the OCI Initiative for Linux containers successfully served a similar purpose. There are now several compatible container runtimes, and synergy effects added to the most obvious effects of standardization. Similarly, there is potential for standardized hypervisor runtime environments that promote portability and allow a standards-compliant virtual (guest) machine to run with significantly less integration efforts.

Hypervisors can fulfill this specification and claim to be compliant with its standard, still leaving opportunity for local optimizations and competitive advantages. Guest virtual machines (VMs) can be engineered to match the specification. In combination, this leads to a better shared industry understanding of how virtualization features are expected to behave, reduced software integration efforts, efficient portability of legacy systems and futureproofing of designs, as well as lower risks when starting product development.

Specification outline
---------------------

The AVPS specification is intended to be immediately usable as a set of platform requirements, 
but also to start the conversation about further standardization and provide guidance for discussions 
between implementers and users of compatible systems using virtualization that follow this specification. 
Each area is therefore split in a discussion section and a requirement section. The requirement section is the normative part. 
(See the section :numref:`Conformance` :ref:`Conformance` for further guidance on adherence to the specification).

Each discussion section outlines various non-normative considerations in addition to the firm requirements. It also provides rationale for the requirements that have been written (or occasionally for why requirements were not written), and it often serves to summarize the state-of-the-art situation in each area.

Hardware considerations
-----------------------

This specification is intended to be hardware selection independent and processor architecture agnostic.

We welcome all input to further progress towards that goal.

This specification targets hardware virtualization where the host, the hypervisor, and the guest use the same CPU instruction set. The code of the guest is directly executed by the processor, and only intercepted by the hypervisor as directed by the processor architecture. No cross-architecture emulation is involved.
We point this out as byte-code interpreters, such as for Java or web-assembly, are inconveniently also called "virtual machines".

Some referenced external specifications are written by a specific hardware technology provider, such as Arm |(r)| [#]_, and define interface standards for the software/hardware interface in systems based on that hardware architecture. We found that some such interface specifications could be more widely applicable – that is, they are not too strongly hardware architecture dependent. The AVPS may therefore reference parts of those specifications to define the interface to virtual hardware. The intention is that the chosen parts of those specifications, despite being published by a hardware technology provider, should be possible to implement on a virtual platform that executes on any hardware architecture.

There are some areas that are challenging, or not feasible, to make hardware independent, such as:

- Access to trusted/secure computing mode (method is hardware dependent).
- Power management (standards are challenging to find).
- Miscellaneous areas, where hardware-features that are designed explicitly to support virtualization are introduced as a unique selling point.

Also, in certain modes, such as access to trusted/secure computing mode for example, note that the software is compiled for a particular CPU architecture as described above. The CPU architecture includes specialized instructions, or values written to special CPU registers, that are used to enter the secure execution mode. Programs in virtual machines should be able to execute such CPU instructions and should not need to be modified to make use of the trusted execution environment when running on a virtual platform.

Continuous work should be done to unify the interfaces in this virtual platform definition to achieve improved hardware portability.

Hardware Devices and Pass-Through
---------------------------------

.. note here that with Trustzone two OSs are running, and TZ is typically
   not described as virtualization.

In a non-virtualization environment, a single operating system kernel accesses the hardware devices. Whereas, a virtual platform based on a hypervisor, typically acts as an abstraction layer over the actual hardware. This layer enables multiple virtual machines (VMs), each running their own operating system, to access a single set of hardware resources. The process of enabling simultaneous access to hardware from (multiple) VMs is called virtualization.

If the hardware is not virtualization-aware, i.e., not designed for multiple independent clients accessing it, then the hypervisor software layer must act as a go-between. The hypervisor exposes multiple virtual hardware interfaces, supported by an underlying device driver implementation that manages access to sequence, parallelize, or arbitrate requests to the physical hardware resource.

Generally, there are multiple ways to provide VMs access to hardware
devices. Hardware devices can be emulated by the hypervisor, presenting the
guest a fully virtual model of a device. This way, the guest can continue
using its driver for the particular hardware, however, device emulation is
typically slower in performance and involves a significant implementation
effort.

Another option is using a virtualization-aware approach for drivers
and virtual devices. The most prominent representative for this approach is
VIRTIO. Here, the hypervisor is offering the VM a virtual device that is
specifically designed to offer the best performance. In this case, the guest
needs a specific driver. However, one driver per device class is sufficient,
where a device class is, for example, network or block storage, as the
mapping to the actual device is done by the hypervisor. This way a VM can be
independent from the specific underlying hardware devices of a platform.

A third option is the so-called "pass-through" approach. This gives a VM
direct access to a hardware device and does not require any emulation or
device driver efforts in the hypervisor. With pass-through, only one VM can
access a device.

One advantage of hardware pass-through is that there is, generally, no performance loss compared to a virtual hardware or emulation devices. In some systems, there may be other reasons for using hardware pass-through, such as reduced implementation complexity. This specification occasionally recommends handling some features using pass-through, but we have found that currently there is no standard for (and little movement towards standardizing) the exact method to configure hypervisors for hardware pass- through. The AVPS has a few general suggestions but does not currently propose a standard for how to make that portable. This may be an open question for future work.

For the purposes of this specification, hardware pass-through is interpreted as allowing direct hardware access for the code executing in the VM. For example, the code (typically part of the operating system kernel which is the owner of all hardware access) manipulates the actual hardware settings directly through memory-mapped hardware registers, or special CPU instructions.

The obvious case for pass-through is if no arbitration or sharing of the hardware resource between multiple VMs is necessary (or feasible), but in some cases it can be beneficial for the Hypervisor to present an abstraction or other type of API to access the hardware, even if this API does not provide shared access to the hardware. The reason is that direct manipulation of registers by VM guests may have unintended side effects on the whole system and other VMs because the hardware does not offer sufficient permission granularity.  In the simplest case, consider that mutually unrelated hardware functions might even be configured by different bits in the same memory-mapped register. This would make it impossible to securely split those functions between two different VMs if normal pass- through access to this register is offered. Instead, the access to this register must be mediated by a Hypervisor, even if it does not support arbitration or sharing of the feature from multiple VMs. If the virtual platform is providing a different interface than direct hardware access on the corresponding native hardware, then it is here still considered virtualization (and it also implies paravirtualization, in fact). In other words, we aim to avoid calling such an implementation pass-through even if it does not enable concurrent access or add any additional features. Like other such virtual interfaces, it is encouraged to also standardize those APIs that give this seemingly “direct” access, but in a different form than the original hardware provides.

.. todo:: There is no pass-through of devices (mostly) which will not require a clock or power management for the devices. We need to add a chapter to give some guidance on clocks and power for pass-through devices.

Virtualization Implementation Designs
-------------------------------------

Comparing virtualization solutions can be difficult due to differing internal designs. Sometimes these are real and effective differences whereas sometimes only different names are used for mostly equivalent solutions, or simply different emphasis is placed on certain aspects.

Some hypervisors start from an emulator for a hardware platform and add native CPU-instruction virtualization features to it. Other platforms match a simple model often assumed in descriptions in this document, in which a single component named “hypervisor” is, in effect, the implementation of the entire virtual platform.

Some others conversely state that their actual hypervisor is minimal, and highlight especially the fact that the purpose of the hypervisor is only to provide separation and scheduling of individual virtual machines – thus it implements a kind of “separation kernel”. The remaining features of those designs might then be delegated to dedicated VMs, either with unique privileges or even VMs that are considered almost identical in nature to the “guest” VMs. Consequently, it is then those provided VMs that implement some of the API of the virtual platform, i.e. the APIs available for use by the “guest” VMs.

Some environments use different nomenclature and highlight the fact that parts of the virtual platform may have multiple privilege levels, “domains”. These are additional levels defined beyond the simple model of: user space < OS kernel < Hypervisor.

Some offerings propose that real-time functions can be run using any Real-Time Operating System (RTOS) that runs as a guest operating system in one VM, as an equivalent peer to a general-purpose VM (running Linux kernel for example). Whereas others put emphasis on their implementation being an RTOS kernel first, that provides direct support for real-time processes/tasks (like an OS kernel), and where the RTOS kernel simultaneously acts as a hypervisor towards foreign/guest kernels. The simple description of this without more deeply defining the actual technical design would be that it is an RTOS kernel that also implements a hypervisor.

The design of the hypervisor/virtualization platform and the design of the full system (including guest VMs) sometimes tend to be interdependent, but the intention of this specification is to try to be independent of such design choices. Although other goals for this specification have also been explained, starting with portability of guest VMs should bring any concerns to the surface. If an implementation can follow the specification and ensure such portability, then the actual technical design of the Hypervisor or virtual platform is free to vary.

These differences of philosophy and design are akin to the discussion of monolithic kernels vs. microkernels in the realm of operating systems but here the discussion is about the “virtual platform kernel” (i.e. hypervisor) instead.

Suffice to say that this specification does not strive to advocate only one approach or limit the design of the virtualization platforms, but still strives to maximize compatibility and shared advancement and therefore focus primarily on defining the APIs between a guest VM (operating system kernel) and the virtual platform it runs on.

Please be aware that certain parts of the discussion sections may still speak about the “Hypervisor” implementing a feature, but it should then be interpreted loosely, i.e. it should fit also designs that provide identical feature compatibility but has the implementation delegated to some type of VM.

Draft specification state
-------------------------

Some areas of the platform are not ready for a firm definition of a standard virtual platform. The reasons could include:

- There are sometimes proposals made upstream in VIRTIO, for example, that appear to fit the needs of a virtual platform but are not yet approved and are still under heavy discussion. The AVPS working group then considered it better to wait for these areas to stabilize.
- The subject is complex and requires further study. Implementations of virtual hardware devices in this area might not even exist for this reason and we therefore defer to a pass-through solution. As an intermediary, the feature may be implemented with a simple hardware driver that exposes a bespoke interface to the VMs but does not make it available to multiple VMs as virtual-hardware and is neither subject for standardization.
- It could be an area of significant innovation and hypervisor implementers therefore wish to be unencumbered by requirements at this time, to either allow for free investigation into new methods, or for differentiating their product.
- The subject area might not have any of the above issues, but the working group has not had time and resources to cover it yet. *This is an open and collaborative process, so input is welcome from additional contributors*.

For situations described above, there are no requirements yet, but we have sometimes kept the discussion section
to introduce the situation, and to prepare for further work. Whereas all parts are open for improvement in later versions, 
some chapters will have the explicit marker

.. todo:: Potential for future work exists here.

We invite volunteers to provide input or write those chapters. Joining the working group discussions would be a welcome first step.

.. rubric:: Footnotes

.. [#] Arm is a registered trademark of Arm Limited (or its subsidiaries) in the US and/or elsewhere
