.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: ../macros.rst
===========================
Automotive Virtual Platform
===========================

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in
[`RFC 2119 <https://datatracker.ietf.org/doc/html/rfc2119>`_].

Architecture
------------

The intended applicability of this specification is any type of computer
systems inside of vehicles, a.k.a. Electronic Control Units (ECUs).

There are no other assumptions about the high-level system architecture
at this time.

.. _Conformance:

Conformance to specification
----------------------------

**Optional features vs. optional requirements.**

The intention of a standard is to maximize compatibility. For that
reason, it is important to discuss how to interpret conformance
(compliance) to this specification. The specification might in its
current state be used either as guidance or as firm requirements,
depending on project agreements. In an actual
automotive end-product, there of course also remains the freedom to
agree on deviations among partners, so the intention of this chapter
is not to prescribe project agreements, but only to define what it
means to follow, or be compliant with this specification.

Several chapters essentially say: “if this feature is implemented…
then it shall be implemented as follows”. The inclusion of this
feature is optional, but adherence to the requirements is not. Note
first that this is the feature of the virtualization platform, not the
feature of an end-user product. Another way to understand this is that
if the mentioned feature exists (in a compliant implementation) then
it shall not be
implemented in an alternative or incompatible way (an exception should
only be made if this is explicitly offered in addition to the
specified way).

The specification may also list some features as mandatory. End-user
products are, as noted, free to include or omit any features, so the
intention here is only to say that if a virtual platform implementation
(wants to claim that it follows the specification, then those features
must be available. While not every such feature might be used in an
end-user product, fulfilling the specification means they exist and are
already implemented as part of the platform implementation, and are
being offered by the hypervisor implementation / virtual platform, to
the buyer/user of that platform.

Virtualization support in hardware
----------------------------------

When running on hardware that supports it, then the virtualization
hardware support for things like interrupts and timers, performance
monitoring, GPU, etc., are generally encouraged to be used. But it
should avoid contradicting the standard APIs as listed here. If in
doubt, the virtual platform shall follow the specification (provide the
feature as specified) and perhaps provide alternatives in addition to it
(see previous discussion Optional features vs. optional requirements).
In some cases, there is no conflict
because the optimized hardware support might be used “below the API”, in
other words in the implementation of the virtual platform components
while still fulfilling the specified API.

Whenever any conflict arises between specific hardware support for
virtualization and the standard virtual platform API, then we strongly
encourage raising this for community discussion to affect future
versions of the specification. It might be possible, through
collaboration, to adjust APIs so that these optimizations can be used,
when the hardware supports it. And if this is not possible, then a
specification like this can still explicitly document alternative
acceptable solutions, rather than non-standard optimizations being
deployed with undocumented/unknown consequences for portability and
other concerns.

Hardware emulation
------------------

A virtualization platform may deal with the emulation of hardware
features for various reasons. Lacking explicit support in the hardware,
it might be the only way to implement hardware device sharing. In
particular, we want to note that for less capable hardware, the
hypervisor (or corresponding part of virtual platform) may need to
implement emulation of some hardware features that it does not have but
which are available on other hardware. It would typically provide much
lower performance, but if semantic compatibility can be achieved with
the virtual platform as it is specified in this document, then this
still supports portability and integration concerns.
