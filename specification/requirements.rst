.. SPDX-License-Identifier: CC-BY-SA-4.0

============
Requirements
============
   
.. toctree::

   requirements/general.rst
   requirements/storage.rst
   requirements/networks.rst
   requirements/graphics.rst
   requirements/audio.rst
   requirements/IOMMU.rst
   requirements/USB.rst
   requirements/AutomotiveNetworks.rst
   requirements/watchdog.rst
   requirements/power_system.rst
   requirements/gpio.rst
   requirements/sensors.rst
   requirements/cameras.rst
   requirements/codecs.rst
   requirements/security.rst
   requirements/supplemental.rst


Note on Requirement ID Numbering
--------------------------------

Requirement IDs are structured as ``AVPS-ID-NUMBER`` where ID is a class
identifier, such as ``NET``, and NUMBER is a consecutive and unique number
within this class, starting at 1. Numbers are never reused such that two
different requirements can never have the same requirements ID.

If a requirement is modified (any modification, even editorial ones), the
new version gets a ``#CHANGENUMBER`` appended. The change number starts with
1, and increases by 1 for every modification made.

Examples of requirement IDs are: ``AVPS-NET-3``, ``AVPS-GEN-10``, ``AVPS-GEN-6#3``.
