.. SPDX-License-Identifier: CC-BY-SA-4.0
.. include:: macros.rst

=======
Preface
=======

This work is licensed under a
`Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License <https://creativecommons.org/licenses/by-sa/4.0/legalcode.en>`_.

:underline:`This document originates from Genivi which became` `COVESA <https://covesa.global>`_.

The resulting readable documentats in HTML and PDF are accessible from `ReadTheDocs webwite <https://automotive-virtual-platform-specification.readthedocs.io/en/latest/>`_.

Editor in Chief
---------------

Adam Lackorzynski <adam.lackorzynski at soafee.io> is currently serving as
Editor in Chief. Feel free to contact via e-mail or through the
`repository facilities <https://gitlab.com/soafee/hypervisor-wg/avps/-/issues>`_.

Document Build Instructions
---------------------------

This document is written in RST and uses Sphinx as a generation engine.

Get it from `here <https://gitlab.com/soafee/hypervisor-wg/avps>`_.

To build the documentation locally, install Sphinx through your package system or follow the `Installing Sphinx <https://www.sphinx-doc.org/en/master/usage/installation.html>`_ instructions.

Once installed, you can run 

.. code-block:: bash
   :caption: Building document variants
  
   # build  html
   make html
  
   # build PDF
   make latexpdf

Contributions to this project are accepted under the same license
with developer sign-off as described in the :ref:`Contributing`.

