# SPDX-License-Identifier: CC-BY-SA-4.0
# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Automotive Virtual Platform Specification'
copyright = 'GENIVI Alliance 2021, SOAFEE 2023-2025, CC-BY-SA 4.0 International'
author = 'GENIVI, SOAFEE Members'
version = '2.0.90'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
        'sphinx_rtd_theme',
        'sphinx.ext.todo'
]

todo_include_todos = True
numfig = True
numfig_format = {'figure': 'Figure %s'}

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '.*.rst']

# -- Latex/PDF contriguration ------------------------------------------------
latex_logo = 'specification/images/soafee_logo.png'
latex_show_urls = 'footnote'

latex_elements = {
    'papersize': r'a4paper',
    # Arm coporate blue encoding
    'sphinxsetup': r'TitleColor={RGB}{0, 145, 189}',
# 'extraclassoptions': r'openany'  is used to open new chapter on any page, otherwise it starts on odd page numbers, resulting in many blank pages; assumes recto-verso print (alternating footers)
# 'extraclassoptions': r'openany, oneside'  is used to open new chapter on any page; assume recto only printing (single footer)
    'extraclassoptions': r'openany',
    'passoptionstopackages': r'\PassOptionsToPackage{svgnames}{xcolor}',
    'preamble': r'''
        \newcommand{\DUroleblack}[1]{{\color[HTML]{000000} #1}}
        \newcommand{\DUrolegray}[1]{{\color[HTML]{808080} #1}}
        \newcommand{\DUrolesilver}[1]{{\color[HTML]{C0C0C0} #1}}
        \newcommand{\DUrolewhite}[1]{{\color[HTML]{FFFFFF} #1}}
        \newcommand{\DUrolemaroon}[1]{{\color[HTML]{800000} #1}}
        \newcommand{\DUrolered}[1]{{\color[HTML]{FF0000} #1}}
        \newcommand{\DUrolemagenta}[1]{{\color[HTML]{FF00FF} #1}}
        \newcommand{\DUrolefuchsia}[1]{{\color[HTML]{FF00FF} #1}}
        \newcommand{\DUrolepink}[1]{{\color[HTML]{FFC0CB} #1}}
        \newcommand{\DUroleorange}[1]{{\color[HTML]{FFA500} #1}}
        \newcommand{\DUroleyellow}[1]{{\color[HTML]{FFFF00} #1}}
        \newcommand{\DUrolelime}[1]{{\color[HTML]{00FF00} #1}}
        \newcommand{\DUrolegreen}[1]{{\color[HTML]{008000} #1}}
        \newcommand{\DUroleolive}[1]{{\color[HTML]{808000} #1}}
        \newcommand{\DUroleteal}[1]{{\color[HTML]{008080} #1}}
        \newcommand{\DUrolecyan}[1]{{\color[HTML]{00FFFF} #1}}
        \newcommand{\DUroleaqua}[1]{{\color[HTML]{00FFFF} #1}}
        \newcommand{\DUroleblue}[1]{{\color[HTML]{0000FF} #1}}
        \newcommand{\DUrolenavy}[1]{{\color[HTML]{000080} #1}}
        \newcommand{\DUrolepurple}[1]{{\color[HTML]{800080} #1}}
        \newcommand{\DUroleunderline}[1]{\underline{#1}}
        \definecolor{ArmCorporateBlue}{RGB}{0, 145, 189}
        '''
        ,
# chapters formatting
        'fncychap': r'''
        \usepackage[Glenn]{fncychap}
        % change chapter numbering color
        \renewcommand{\CTV}[1]{\color{ArmCorporateBlue}\Huge\bfseries}
        % change chapter text color
        \renewcommand{\CNV}[1]{\color{ArmCorporateBlue}\Huge\bfseries}
        % ensure 'Contents' from table of contents do not have the surrounding square
        \renewcommand{\DOTIS}[1]{\color{ArmCorporateBlue}\Huge\bfseries #1}
        ''',
#        'fncychap': r'\usepackage[Bjornstrup]{fncychap}',
#        'fncychap': r'',
}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_logo = 'specification/images/soafee_logo.svg'
# Don't show the "Built with Sphinx" footer
html_show_sphinx = False
